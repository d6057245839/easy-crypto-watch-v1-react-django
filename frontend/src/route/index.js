import React from "react";
import { Routes, Route } from "react-router-dom";
import { Navigate } from "react-router-dom";

import AuthLogin3 from "pages/auth/login";
import AuthRegister3 from "pages/auth/register";
import Homepage from "pages/homepage";
import Dashboard from "pages/dashboard";
import Edit from "pages/dashboard/edit";

import AuthGuard from "route/route-guard/AuthGuard";
import GuestGuard from "route/route-guard/GuestGuard";
//import Temp from 'views/temp/Temp';

function AllRoutes() {
  return (
    <Routes>
      <Route exact path="*" element={<Navigate to="/" />} />
      <Route exact path="/" element={<Homepage />} />
      {/* Test Page */}
      {/* <Route exact path="/temp" element={<Temp/>} /> */}
      <Route exact path="/auth" element={<AuthGuard />}>
        <Route exact path="/auth/login" element={<AuthLogin3 />} />
        <Route exact path="/auth/register" element={<AuthRegister3 />} />
      </Route>
      <Route exact path="/dashboard" element={<GuestGuard />}>
        <Route exact path="/dashboard" element={<Dashboard />} />
        <Route exact path="/dashboard/edit" element={<Edit />} />
      </Route>
    </Routes>
  );
}

export default AllRoutes;
